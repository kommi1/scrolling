import React, { Component } from 'react';
import { Image, View, Text, TextInput, ToastAndroid, TouchableOpacity, Keyboard, ScrollView, FlatList, TouchableHighlight, Dimensions, KeyboardAvoidingView,SectionList } from 'react-native';

let width = Dimensions.get("window").width;
let Height = Dimensions.get("window").height;
let obj = [{ name: "naveen", age: 22, isDefault: true },
{ name: "praveen", age: 23, isDefault: false },
{ name: "maveen", age: 24, isDefault: false },
{ name: "ravi", age: 25, isDefault: false },
{ name: "kishore", age: 26, isDefault: false },
{ name: "prabha", age: 28, isDefault: false },
{ name: "prathap", age: 27, isDefault: false },
{ name: "kiran", age: 26, isDefault: false },
{ name: "sanju", age: 21, isDefault: false },
{ name: "prasad", age: 23, isDefault: false },
{ name: "rafi", age: 23, isDefault: false }
];
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      username: '',
      name: "",
      visible: false,
      enableScroll: false,
      selectedName: "",
      sectionVisible:false,
    }
  }
  UNSAFE_componentWillMount() {
    obj.map((ob) => {
      if (ob.isDefault === true) {
        this.setState({ selectedName: ob.name });
      }
    })

  }
  _renderRow = (rowData) => {
    return (
      <TouchableOpacity style={{ width: '90%', marginTop: 5, marginBottom: 5, backgroundColor: 'gray' }}
        onPress={() => {
          this.setState({
            selectedName: rowData.name,
            visible: false
          })
        }
        }>
        <Text style={{ fontSize: 20, textAlign: "center" }}>{rowData.name}</Text>
      </TouchableOpacity>
    );
  }
  validate() {
    //include your validation inside if condition
    if (this.state.password == "yourname") {
      Keyboard.dismiss();
      ToastAndroid.show("Success", ToastAndroid.SHORT)
      // navigate to next screen
    }
    else {
      Keyboard.dismiss();
      ToastAndroid.show("Wrong password, try again", ToastAndroid.SHORT)
    }
  }
  focusChange = () => {
    console.log("hjbkjwdbhjhwjubhjnwnjbh");
    if (this.state.username.length >= 5) {
      console.log("good")
    }
    else {
      this.setState({ wrong: "please enter minimum 5 characters" })
    }
  }

  render() {
    var empty = (
      <View>
        <Text></Text>
      </View>
    );
    var listedviews = (
      <View style={{ height: 150, width: width - 20, backgroundColor: "#cdcdcd",marginBottom:20}}
        // onStartShouldSetResponderCapture={
        //   () => {
        //     if (this.state.enableScroll === false) {
        //       this.setState({ enableScroll: !this.state.enableScroll })
        //     }
        //   }
        // }
        >
        <FlatList
          data={obj}
          renderItem={({ item}) =>(<TouchableOpacity style={{ width: '100%', marginTop: 5, marginBottom: 5, backgroundColor: 'gray' }}
          onPress={() => {
            this.setState({
              selectedName:item.name,
              visible: false
            })
          }
          }>
          <Text style={{ fontSize: 20, textAlign: "center" }}>{item.name}</Text>
        </TouchableOpacity>)}
        keyExtractor={(item,index)=>index}
        />
      </View>
    );
    var sectionList=(<View style={{ height: 150, width: width - 20, backgroundColor: "#cdcdcd",marginBottom:20,marginTop:10}}
    // onStartShouldSetResponderCapture={
    //   () => {
    //     if (this.state.enableScroll === false) {
    //       this.setState({ enableScroll: !this.state.enableScroll })
    //     }
    //   }
    // }
    >
    <SectionList
      sections={[{title:'N',data:["Naveen","Narayana","Naresh","Nagarjuna","Nayak"]},
    {title:'P',data:["Prasad","Praveen","Praneetha","Pravallika","Pranay"]}]}
      renderItem={({ item}) =>(<TouchableOpacity style={{ width: '100%', marginTop: 5, marginBottom: 5, backgroundColor: 'gray' }}
      onPress={() => {
        this.setState({
          selectedName:item.name,
          visible: false
        })
      }
      }>
      <Text style={{ fontSize: 20, textAlign: "center" }}>{item}</Text>
    </TouchableOpacity>)}
    renderSectionHeader={({section})=>{ return <TouchableOpacity style={{width: '100%', marginTop: 5, marginBottom: 5, backgroundColor: 'gray'}}><Text style={{textAlign:"center",color:"black",fontSize:20}}>{section.title}</Text></TouchableOpacity>}}
   keyExtractor={(item,index)=>index}
    />
  </View>);
    return (

      <View style={{  backgroundColor: 'green',flex: 1,alignItems:"center"}}>
         <View style={{width:width-20,}}>
        <ScrollView scrollEnabled={true}>
        <KeyboardAvoidingView>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <View style={{height:30,width:'100%'}}></View>
              <TextInput
                style={{ width: width-20, backgroundColor: 'white' }}

                secureTextEntry={false}

                value={this.state.username}
                autoFocus={false}
                onChangeText={(username) => this.setState({ username })}
                onSubmitEditing={() => { this.focusChange }}
              />
              <TouchableOpacity style={{width: '50%', marginTop: 5, marginBottom: 5, backgroundColor: 'violet',padding:10}}
              onPress={()=>{
                this.setState({sectionVisible:! this.state.sectionVisible})
              }}><Text style={{textAlign:"center"}}>Section list</Text></TouchableOpacity>
              {this.state.sectionVisible === true ? sectionList : empty}
               

              <TextInput
                style={{ width:width-20, backgroundColor: 'white' }}
                returnKeyType="go"
                secureTextEntry
                autoCapitalize="none"
                autoCorrect={false}
                autoFocus={false}
                onChangeText={(password) => this.setState({ password })}

              />
              <TouchableOpacity onPress={() => this.validate()}
                style={{ padding: 15, backgroundColor: 'rgb(102, 192, 231)', width: 100, margin: 20, alignItems: "center" }}>
                <Text>Next</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { ToastAndroid.show("hello Naveen", ToastAndroid.SHORT) }} style={{ width: 100, backgroundColor: "red", marginBottom: 20 }}><Text style={{textAlign:"center"}}>click</Text></TouchableOpacity>
              <TouchableHighlight
                style={{ width: width - 20, height: 50, marginLeft: 12, marginRight: 12, }}
                onPress={() => {
                  this.setState({ visible: !this.state.visible })
                }}
                underlayColor="white"
              >
                <View style={{ width: width - 20, height: 50, backgroundColor: "yellow", justifyContent: "space-between", alignItems: 'center', flexDirection: 'row' }}>
                  <View>
                    <Text style={{ fontSize: 20, marginLeft: 5 }}>{this.state.selectedName}</Text>
                  </View>
                  <View>
                    <Image style={{ width: 16, height: 16, marginRight: 5 }} source={this.state.visible == false ? require('./img/down_arrow.png') : require('./img/up_arrow.png')} />
                  </View>
                </View>
              </TouchableHighlight>
              {this.state.visible == true ? listedviews : empty}
              <TextInput
                style={{ width:width-20, backgroundColor: 'white' }}

                secureTextEntry={false}

                value={this.state.username}
                autoFocus={false}
                onChangeText={(username) => this.setState({ username })}
                onSubmitEditing={() => { this.focusChange }}
              />
              <TextInput
                style={{ width: width-20, backgroundColor: 'white' }}
                returnKeyType="go"
                secureTextEntry
                autoCapitalize="none"
                autoCorrect={false}
                autoFocus={false}
                onChangeText={(password) => this.setState({ password })}

              />
              <TouchableOpacity onPress={() => this.validate()}
                style={{ padding: 15, backgroundColor: 'rgb(102, 192, 231)', width: 100, margin: 20, alignItems: "center" }}>
                <Text>Next</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { ToastAndroid.show("hello Naveen", ToastAndroid.SHORT) }} style={{ width: 100, backgroundColor: "red", marginBottom: 20 }}><Text style={{textAlign:"center"}}>click</Text></TouchableOpacity>
              <TextInput
                style={{ width: width-20, backgroundColor: 'white' }}

                secureTextEntry={false}

                value={this.state.username}
                autoFocus={false}
                onChangeText={(username) => this.setState({ username })}
                onSubmitEditing={() => { this.focusChange }}
              />
      
              <TextInput
                style={{ width: width-20, backgroundColor: 'white' }}
                returnKeyType="go"
                secureTextEntry
                autoCapitalize="none"
                autoCorrect={false}
                autoFocus={false}
                onChangeText={(password) => this.setState({ password })}

              />
              <TouchableOpacity onPress={() => this.validate()}
                style={{ padding: 15, backgroundColor: 'rgb(102, 192, 231)', width: 100, margin: 20, alignItems: "center" }}>
                <Text>Next</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { ToastAndroid.show("hello Naveen", ToastAndroid.SHORT) }} style={{ width: 100, backgroundColor: "red", marginBottom: 20 }}><Text style={{textAlign:"center"}}>click</Text></TouchableOpacity>
              <TouchableHighlight style={{width:width-20,height:30,justifyContent:'center',backgroundColor:"violet"}} onLongPress={()=>{ToastAndroid.show("Naveen You Long pressed the Button",ToastAndroid.SHORT)}} underlayColor="white" >
                <Text style={{textAlign:"center"}}>Long press</Text>
              </TouchableHighlight>
              <View style={{height:30,width:'100%'}}></View>
              
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
        </View>
      </View>

    );
  }
}